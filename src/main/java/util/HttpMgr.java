package main.java.util;

import java.io.IOException;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class HttpMgr {
	OkHttpClient client;
	StringBuilder strBuilder = new StringBuilder(1000);
	public final String akcode = "GxPPCwDgE2ofakPG9uBnjpNRbIgOwGZi";
	public HttpMgr() {
		client = new OkHttpClient();

	}
	
	public String getSuggestion(String queryStr, String regionStr) {
//		queryStr = "外海中华大道74号202";
//		regionStr = "江门";		
		
//		http://api.map.baidu.com/place/v2/search?query=ATM机&tag=银行&region=北京&output=json&ak=OO6U1zyyp6EEGvwCqdOlptI2ALiMVz1I //GET请求
//		String urlStr = "http://api.map.baidu.com/place/v2/suggestion?query=" + queryStr + "&region=" + regionStr + "&city_limit=true&output=json&ak=OO6U1zyyp6EEGvwCqdOlptI2ALiMVz1I";
//		String urlStr = "http://api.map.baidu.com/place/v2/suggestion?query=江门市邦民路&region=江门&city_limit=true&output=json&ak=OO6U1zyyp6EEGvwCqdOlptI2ALiMVz1I";

//		这是地址完整
//		String urlStr = "http://api.map.baidu.com/place/v2/suggestion?query=" + queryStr + "&region=" + regionStr + "&city_limit=true&output=json&ak=OO6U1zyyp6EEGvwCqdOlptI2ALiMVz1I";
//		http://api.map.baidu.com/place/v2/search?query=银行&location=39.915,116.404&radius=2000&output=xml&ak=您的密钥 //GET请求
		
//		strBuilder.append("http://api.map.baidu.com/place/v2/search?query=");
//		strBuilder.append(queryStr);
//		strBuilder.append("&region=广东省&tag=住宅区&city_limit=true&scope=2&output=json&ak=dcTB6Fh7ohfGKFGpGCOS9jzLe2SwYPU0");
//		System.out.println("strbuild = " + strBuilder.toString());
				
		strBuilder.append("http://api.map.baidu.com/place/v2/suggestion?query=");
		strBuilder.append(queryStr);
		strBuilder.append("&region=");
		strBuilder.append(regionStr);
		strBuilder.append("&city_limit=true&output=json&ak=");
		strBuilder.append(akcode);
//		
		
//		String urlStr = "http://api.map.baidu.com/place/v2/search?query=" + queryStr + "&scope=1&region=" + regionStr + "&output=json&ak=OO6U1zyyp6EEGvwCqdOlptI2ALiMVz1I";
//		String urlStr = "http://api.map.baidu.com/place/v2/detail?uid=fad4a85b553ed66bfea7787a&output=json&scope=2&ak=OO6U1zyyp6EEGvwCqdOlptI2ALiMVz1I";
		
		//现在先把外部逻辑写好。
		//先遍历整个地址
		Request request = new Request.Builder().url(strBuilder.toString()).build();
		strBuilder.setLength(0);

		try {
			Response res = client.newCall(request).execute();	
//			System.out.println(res.body().string());
			return res.body().string();
		}catch(IOException e) {
			
		}
		strBuilder.setLength(0);
		return null;
	}
}
