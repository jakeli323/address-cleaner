package main.java.util;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Pattern;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import main.java.util.json.JSONArray;
import main.java.util.json.JSONException;
import main.java.util.json.JSONObject;

/**
 * 根据街道库信息.xlsx初始化的，用于根据譬如“岐安里”获取包含该街道的“道路信息”或者“区域信息”，譬如“江华路”，“蓬江区”
 *
 */
public class AddressProcessor {
	private String filePath;
	private HttpMgr httpMgr; 
	private String ChNumRepo = "一二三四五六七八九十";
	private String ChUnitRepo = "号栋幢、之";
	private final String fieldName = "自动识别地址";
	private final int kEndIndex = 22000; //匹配count行
	private final int kStartIndex = 11150;//从第xx行开始
	

//	待处理文档，  httpmgr，   
	
	public AddressProcessor(String path) {
		filePath = path;
		httpMgr = new HttpMgr();
//		getFields("外海中华大道74号202");
	}
	
	public void process() {
		try {
			InputStream is = new FileInputStream(filePath);
//			System.out.println("11111111111");
			Workbook wb = new XSSFWorkbook(is);
//			Workbook outWb = new XSSFWorkbook();

			XSSFSheet sheet = (XSSFSheet) wb.getSheetAt(0);		

			Iterator<Row> rowIter = sheet.iterator();
			int i = 0;
			int start = kStartIndex;
			while(rowIter.hasNext()) {
				Row row = rowIter.next();

				if(i < start) {
					i++;
					is.close();

					continue;
				}
				Cell cell = row.getCell(1);
				String address = "";
				String originAddress = "";

				if(cell.getCellTypeEnum() == CellType.STRING) {
					address = cell.getStringCellValue();
					if(address.equals("\\N") ) address = "";
					originAddress = new String(address);
//					System.out.println("is string yyyyyyyyyyyyyyy");
					
					
					// 根据百度地图api的suggestion服务，也就是地址提示服务，获取完整地址。
//					String[] addressFields = getFields(address);
//					if(addressFields.length == 0) {
					if(address.length() > 0) {
						address = trimAddress(address);	
						
						String[] addressFields = getFields(address);
						
						if(addressFields.length > 0) {
							// 譬如缺省 广东省 江门市，那么就填上从江门市开始遍历，沾到地址的前面。如果已经有江门市了，就不用站过去。
							
							address = completeAddress(originAddress, addressFields);
							
							if(!address.contains("省")) {
								address = "广东省" + address;
							}
						}else {
							address = originAddress;
						}	
						

					}			
				}else {
//					System.out.println("is  not string nnnnnnnnnnnnnnnnnn");
				}


				
				System.out.println("--------------------------------------     " + i);
				Cell newCell = row.createCell(3);
				newCell.setCellType(CellType.STRING);

				newCell.setCellValue(address);
				
				if(i==0) { 
					newCell.setCellValue(fieldName);
				}
//				outWb.write(os);
				is.close();

				
				FileOutputStream fos = new FileOutputStream(new File(filePath));
		        wb.write(fos);
		        
		        fos.close();
//				System.out.println("address  =  " + address);
				i++;

				if(i > kEndIndex) break;				
			}
			
			//做一个post请求，测一下 api接口
			
		}catch(IOException e) {
			System.out.println("文件找不到：" + filePath);
		}	
		
		
		
		
	}
	
	private String completeAddress(String address, String[] fields) {
//		for(String s : fields) {
//			if(!address.contains(s)) {
//				address = s + address;
//			}
//		}
		
		if(fields.length != 2) {
			return address;
		}
//		System.out.println("fiels = " + fields.toString());
		String district = fields[0];
		String city = fields[1];
		System.out.println("d = " + district);
		System.out.println("dcccc = " + city);
		System.out.println("address ============= " + address);
		int provinceIndex = address.indexOf("省");
		
		System.out.println("province indx = " + provinceIndex);
		int insertIndex = 0;
		
		String targetProvince = "";
		String targetCity = "";
		String targetDistrict = "";
		
		if(provinceIndex > 0) {
//			insertIndex = provinceIndex;
			targetProvince = address.substring(0, provinceIndex + 1);
			
			address = address.substring(provinceIndex + 1);
//			截出 省 存在另外一个地方
			
			System.out.println("origin province  = " + targetProvince);
			System.out.println("address = " + address);
		}else {
			targetProvince = "广东省";
			System.out.println("province else");
		}
		
		int cityIndex = address.indexOf("市");
		System.out.println("111111111111111111");
		
		
		targetCity = city;

		if(cityIndex > 0) {
//			insertIndex = cityIndex;
//			originCity = address.substring(0, cityIndex + 1);
//			originCity = city;

			address = address.substring(cityIndex + 1);
			System.out.println("origin city   = " + targetCity);
			System.out.println("address = " + address);
			//截出市 存在另外一个地方
		}
//		else {
//			originCity = city;
//			System.out.println("dictye else");
//		}
		
		System.out.println("1222222222222222222222222 add" + address);
		int districtIndex = address.indexOf("区");
		
		
		targetDistrict = district;
		if(districtIndex > 0) {
//			insertIndex = districtIndex;
//			originDistrict = address.substring(0, districtIndex + 1);			
			address = address.substring(districtIndex + 1);
			System.out.println("origin district   = " + targetDistrict);
			System.out.println("address = " + address);			
			//截出区存在另外一个地方
		}
//		else {
//			originDistrict = district;
//			
//			
//			System.out.println("disctit else");
//		}
//		
		System.out.println("jfijwqepfoqjwpeiofj add = " + address);
		System.out.println("origin city  = " + targetCity);
		System.out.println("pro = " + targetProvince);
		
		address = targetProvince + targetCity + targetDistrict + address;
		
		
		
		
		System.out.println("ccccccccccccccccced  = " + address);
		
		return address;
	}
	
	private String trimAddress(String address) {
		int maxIndex = address.length() - 1;		
		int startTrimIndex = maxIndex;
//		ChNumRepo.contains(String.valueOf(address.charAt(max-1)))
		while(checkToTrimRepo(address.charAt(startTrimIndex))) {
			startTrimIndex--;
			if(startTrimIndex <= 0) return null;
		}
		
		
		address = address.substring(0, startTrimIndex + 1);
//		System.out.println("trimmed !!!!!!! = " + address);
		
		return address;
	}
	
	private boolean checkToTrimRepo(Character c) {
		boolean ret = false;
		if(ChNumRepo.contains(String.valueOf(c))) ret = true;
		if(ChUnitRepo.contains(String.valueOf(c))) ret = true;
		if(Character.isDigit(c)) ret = true;
		
		return ret;
	}
	
	public String[] getFields(String address) {
		String[] ret = new String[0];
		List<String> strList = new ArrayList<String>();
		System.out.println("原地址  = " + address);
		String res = httpMgr.getSuggestion(address, "江门市");
//		System.out.println("json = " + res);
		if(res == null) {
			return ret;
		}
		

		try {
			JSONObject json = new JSONObject(res);
			
			JSONArray jsonArr = json.getJSONArray("result");
//			System.out.println("json arr  = " + jsonArr.toString());
			if(jsonArr.length() == 0) {
				System.out.println("--------------------------------------");

				return ret;
			}
		
			JSONObject jsonObj = jsonArr.getJSONObject(0);
			String district = jsonObj.getString("district");
			strList.add(district);		
			String city = jsonObj.getString("city");
			//todo 优化
			//如果发现号 一栋两栋 幢  向前 找 包含前面的数字， 一起干掉这些 在匹配
			//原地址  = 蓬江区岭江一品华府11幢
			//原地址  = 江门市蓬江区西江悦府14幢、甘源路27号、甘宁路21号
			strList.add(city);
			

				
//			System.out.println("识别后的区域 = " + strList.toString());

			return strList.toArray(new String[0]);			
			
		}catch(JSONException e) {
			
		}
		return ret;
	}
}
